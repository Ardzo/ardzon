# Version history:

## **2.1.1.1** 2018-11-19
* Value of `$ardzon_data_folder` in README.md is changed in case of SBS "Web-Pro".

## **2.1.1** 2018-11-19
* Value of `ardzon_external_css` is changed in case of SBS "Web-Pro".

## **2.1** 2018-11-11
* Fixed a defining of form of "textarea".
* Fixed a position of context menu in case of scrolled iframe.
* Improved and fixed a paths in script of uploading of pictrures.
* Value of variable `ardzon_path_pics` in case of SBS "Web-Pro" is fixed.
* View of options of context menu is fixed (CSS).
* Minor improvement in CSS of toolbar.

## **2.0.1** 2018-11-10
* Fixed `"main.php"` for work with SBS "Web-Pro".

## **2.0** 2018-10-31
* Refactoring, transition from PHP to JavaScript.
* Using is improved and simplified.
* All dialog windows calls with JavaScript instead of PHP.
* Adapted for using with PHP 7.
* Filtering and shielding of JavaScript code is added.
* Work with anchors in links (if only anchor in link) is fixed.
* Ability of setting of class for any fragment instead of block element.
* "Cut" and "copy" with toolbar is fixed.
* Support of visual themes is added: includes 3 themes.
* UI is improved.
* p-formatting is added.
* Control of id of elements is added.
* Flash support is removed.
* Some other minor fixes.

## **1.5.2** 2018-10-20
* Minor fixes in "get_window.php", "main.php".

## **1.5.1** 2018-10-19
* Minor fixes in "lib.php".

## **1.5** 2015-11-19
* Library JQuery used instead of eAjax.

## **1.4.2** 2013-10-14
* Call of "session_register()" is removed for compatibility with PHP 5.4+.

## **1.4.1** 2010-03-12
* Creating of hyperlink in IE is fixed.

## **1.4** 2010-02-28
* Full support of Firefox is added.
* Dialog windows now uses Ajax.
* Partial support of Opera, Safari, Chrome.
* Loading of default language if any other not found.
* Working with parameters of tables is improved.
* Hiding of toolbar in HTML mode.
* Forgotten CSS file of previous version is included in package.
* "Cellspacing" and "Cellpadding" is placed correctly in window of properties of table.
* Pasting of clipboard data with button of toolbar is fixed (IE only).
* Search is added (IE only).
* View of toolbar at narrow screen in IE7 is fixed.
* Background highlighting of text in Firefox is added.
* Other small improvements.

## **1.3.1b** 2009-07-10
* Color scheme is changed.
* Window of adding of image and flash is increased by height.

## **1.3b** 2008-11-25
* Automatic removing of absolute parts of URLs of hyperlinks and images is added.
* Adding hyperlink without protocol ("HTTP://" etc.) now.
* Resizing of images is fixed.
* Some changes in placement.
* Language files uses UTF-8 now.

## **1.2b** 2007-11-27
* Possibility to change "class" in images is added.

## **1.1.1b** 2007-06-09
* Font size of server script line is fixed instead of percentage.

## **1.1b** 2007-05-31
* Can work with HTML containing server side code and JavaScript code.
* Work with "\n", "\r" is fixed: it not cutted now.

## **1.0.3b** 2006-12-29
* Name of folder for pictures for SBS "Web-Pro" is changed.

## **1.0.2b** 2006-05-25
* Function "get_wysiwyg_field()" can correctly accept zero width and height parameters.

## **1.0.1b** 2006-05-11
* Context menu fixed for Firefox.

## **1.0b** 2006-05-03
* This is beta version.
