# Ardzon. WYSYWIG editor for website

## Introduction
Ardzon is a simple and effective online WYSIWYG editor for using at websites with PHP support.  
It generally uses JavaScript only.
PHP is using only for uploading of pictures. You can easily adapt that script for using with other server languages.  
**Ardzon takes 200k of code only!**

## Requirements
* Server side:
	* PHP 5.0+ and also PHP 7
* Client side:
	* Modern Gecko browser. Tested in Firefox, Chrome, Opera. Work in IE and Edge is not guarantied.

Also requires libraries JQuery 1.7+ (tested in 3.3.1), JQuery Form Plugin, Ardzo JSPlus 0.5.1+ - all of it is included in package.

## Installation and first steps
You can use file from folder `"example"` for testing and understanding:

* copy `"index.php"` from that folder into folder of your project;
* create folder "`data`" in folder of project and give it permission to write - this folder stores an uploaded pictures;
* copy folder `"ardzon"` from `"src"` into folder of your project.

Start `"index.php"` in a browser, that's it!

Additionally you can set a variables in `"ardzon/lib_main.js"` if necessary.

If you use editor with SBS "Web-Pro" (U0 V6.13+):  
- uncomment group of variables in `"ardzon/lib_main.js"`;  
- set `$ardzon_data_folder = '/sbs_wp/data/wysiwyg_pictures/';` in `"ardzon/main.php"`.

See additional info at http://ardzon.ardzo.com

## Using and settings

### Calling without sizes
You can call editor without specifying of width and height. In this case editor will adapt to width and height of textarea.
But this not works if size of textarea is specified in percents.

### Changing of themes
Ardzon includes 3 visual themes: Native, Air and Dark. You can also easily add your themes.
For changing a current theme change a call of CSS in project file (see `"example/index.php"`) and change variable `ardzon_path_pics` in `"lib_main.js"`.

### Using of own CSS for work area of editor
Work area is that place where user edits a content. Technically this is separate HTML document and it uses own CSS.  
Ardzon can use 2 CSS files for work area. One is built-in (variable `ardzon_internal_css`) and used always, and another (`ardzon_external_css`) that
you can specify additionally.  
In most cases is enough to use only built-in - it can be changed if needed. 
As additional CSS is suits to specify address of CSS of project - it will be used wholly without changing.


