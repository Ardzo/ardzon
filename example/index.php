<!DOCTYPE html>
<html>
<head>
	<link href="ardzon/themes/native/style.css" rel="STYLESHEET" type="text/css" />
</head>
<body>
	<script type="text/javascript" src="ardzon/lib_main.js"></script>

	<form action="index.php" method="post">
	First field:<br />
	<textarea name="some_field_1" id="field1" cols="90" rows="10"><?php echo @$_POST['some_field_1'] ?></textarea>
	<script type="text/javascript">
		Ardzon.run('field1');
	</script>

	<p />
	Second field:
	<textarea name="field[two]" id="field2id"><?php echo @$_POST['field']['two'] ?></textarea>
	<script type="text/javascript">
		Ardzon.run('field2id', {'width':'90%', 'height':'250px'});
	</script>

	<input type="submit" />
	</form>
</body>
</html>
