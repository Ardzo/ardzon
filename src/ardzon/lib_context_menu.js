/**
* WYSIWYG Editor "Ardzon"
*
* Library for work with context menu
*
* @copyright Copyright (C) 2006 - 2018 Richter
* @author Richter (richter@wpdom.com)
*/

ContextMenu = {
  // "event" need for FF
  Build: function (event, field_id) {
    this.field_id = field_id;

    // Coordinates
    if (isGecko) {
      x = event.pageX;
      y = event.pageY;
    }
    if (isIE) {
      x = frames['ardzon_'+field_id+'_frame'].window.event.x;
      y = frames['ardzon_'+field_id+'_frame'].window.event.y;
    }
    x += document.getElementById('ardzon_'+field_id+'_frame').offsetLeft;
    y += document.getElementById('ardzon_'+field_id+'_frame').offsetTop;

    y -= document.getElementById('ardzon_'+field_id+'_frame').contentWindow.document.body.scrollTop;

    // Creating if not exists
    if (!document.getElementById('ardzon_'+field_id+'_contextmenu')) {
      if (isIE) {
        EditField[field_id].onclick = function() { ContextMenu.Hide(); }
        EditField[field_id].onkeydown = function() { ContextMenu.Hide(); }
        EditField[field_id].onkeypress = function() { ContextMenu.Hide(); }
        document.onclick = function() { ContextMenu.Hide(); }
        document.onkeydown = function() { ContextMenu.Hide(); }
        document.onkeypress = function() { ContextMenu.Hide(); }
      }
      if (isGecko) {
        EditField[field_id].addEventListener('click', function(){ContextMenu.Hide()}, true);
        EditField[field_id].addEventListener('keydown', function(){ContextMenu.Hide()}, true);
        EditField[field_id].addEventListener('keypress', function(){ContextMenu.Hide()}, true);
        document.addEventListener('click', function(){ContextMenu.Hide()}, true);
        document.addEventListener('keydown', function(){ContextMenu.Hide()}, true);
        document.addEventListener('keypress', function(){ContextMenu.Hide()}, true);
      }
      contextMenuDiv = document.createElement("div");
      contextMenuDiv.setAttribute("id", "ardzon_"+field_id+"_contextmenu");
      contextMenuDiv.className = "ardzon_contextmenu";
      contextMenuDiv.style.position = 'absolute';

      var MenuContent = "";
      MenuContent += this.AddItem('bold', Lng._LNG_ARDZON_BOLD_, 30);
      MenuContent += this.AddItem('italic', Lng._LNG_ARDZON_ITALIC_, 450);
      MenuContent += this.AddItem('underline', Lng._LNG_ARDZON_UNDERLINE_, 1230);
      MenuContent += this.AddDivider();
      MenuContent += this.AddItem('cut', Lng._LNG_ARDZON_CUT_, 180);
      MenuContent += this.AddItem('copy', Lng._LNG_ARDZON_COPY_, 150);
      MenuContent += this.AddItem('paste', Lng._LNG_ARDZON_PASTE_, 690);
      MenuContent += this.AddItem('filterpaste', Lng._LNG_ARDZON_FILTERPASTE_, 270);
      MenuContent += this.AddDivider();
      MenuContent += this.AddItem('insertimage', Lng._LNG_ARDZON_IMAGE_, 480);
      MenuContent += this.AddDivider();
      MenuContent += this.AddItem('inserttable', Lng._LNG_ARDZON_TABLE_, 930);
      MenuContent += this.AddItem('table_properties', Lng._LNG_ARDZON_TABLEPROP_, 1170);
      MenuContent += this.AddItem('delete_table', Lng._LNG_ARDZON_DELTABLE_, 960);
//      MenuContent += this.AddItem('insert_row_before', Lng._LNG_ARDZON_INSROWBEFORE_, 1140);
      MenuContent += this.AddItem('insert_row_after', Lng._LNG_ARDZON_INSROWAFTER_, 1110);
      MenuContent += this.AddItem('delete_row', Lng._LNG_ARDZON_DELROW_, 1020);
//      MenuContent += this.AddItem('insert_column_before', Lng._LNG_ARDZON_INSCOLBEFORE_, 1080);
      MenuContent += this.AddItem('insert_column_after', Lng._LNG_ARDZON_INSCOLAFTER_, 1050);
      MenuContent += this.AddItem('delete_column', Lng._LNG_ARDZON_DELCOL_, 990);
//      MenuContent += this.AddItem('split_cell', Lng._LNG_ARDZON_SPLITCELL_, 1200);
      contextMenuDiv.innerHTML = MenuContent;

      contextMenuDiv.style.left = x+'px';
      contextMenuDiv.style.top = y+'px';
      document.body.appendChild(contextMenuDiv);
    }
    contextMenuDiv.style.left = x+'px';
    contextMenuDiv.style.top = y+'px';
    contextMenuDiv.style.display = "block";

    if (isGecko) event.preventDefault();
    return false;
  },

  AddItem : function (command, title, pos_x) {
    return ' \
	<div class="ardzon_contextmenu_option"> \
		<a href="javascript:void(0);" class="icon" style="background-image:url('+ardzon_path_pics+'buttons.gif); background-position: -'+pos_x+'px 50%"></a> \
		<a href="javascript:void(0);" class="text" onclick="Ardzon.processAction(\''+command+'\', \''+this.field_id+'\');">'+title+'</a> \
	</div> \
    ';
  },

  AddDivider : function () {
    return '<div class="ardzon_contextmenu_divider"></div>';
  },

  Hide: function ()
  {
    contextMenuDiv.style.display = "none";
  }
}