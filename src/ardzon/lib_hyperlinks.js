/**
* WYSIWYG Editor "Ardzon"
*
* Library for work with hyperlinks
*
* @copyright Copyright (C) 2006 - 2018 Richter
* @author Richter (richter@wpdom.com)
*/

// Addes a hyperlink
function AddHyperlink(field_id)
{
  var NewObj       = document.createElement('A');

  href = document.forms['hyperlink_form'].elements['url'].value;
  if (href.substr(0, 1) == '#') href = '~'+href;
  hl.href = href;
  NewObj.href      = href;
  NewObj.className = document.forms['hyperlink_form'].elements['_class'].value;
  NewObj.target    = document.forms['hyperlink_form'].elements['_target'].value;
  NewObj.title     = document.forms['hyperlink_form'].elements['_title'].value;
  NewObj.id        = document.forms['hyperlink_form'].elements['_id'].value;

  if (isGecko || isPresto) {
    sel = document.getElementById('ardzon_'+field_id+'_frame').contentWindow.getSelection();
    range = sel.getRangeAt(0);
    tmp = range.startContainer.parentNode;
    if (sel == '') {
      if (document.forms['hyperlink_form'].elements['_text'].value)
        NewObj.innerHTML = document.forms['hyperlink_form'].elements['_text'].value;
      else NewObj.innerHTML = NewObj.href;
    } else NewObj.innerHTML = sel;

    sel.removeAllRanges();
    range.deleteContents();
    range.insertNode(NewObj);
  } else {
    if (Ardzon.CurSelection.type == "Control") {
      NewObj.innerHTML = Ardzon.CurSelection(0).outerHTML;
      Ardzon.CurSelection(0).outerHTML = NewObj.outerHTML;
    } else {
      NewObj.innerHTML = (Ardzon.CurSelection.htmlText=='')?NewObj.href:Ardzon.CurSelection.htmlText;
      Ardzon.CurSelection.pasteHTML(NewObj.outerHTML);      
    }
  }
  JSPcloseWindow('ardzon_modal_win');
}

// Updates an hyperlink (transmitter)
function updateHyperlink(field_id)
{
  href = document.getElementById('url').value;
  if (href.substr(0, 1) == '#') href = '~'+href;
  hl.href = href;
  hl.target = document.getElementById('_target').value;
  hl.title = document.getElementById('_title').value;
  hl.className = document.getElementById('_class').value;
  hl.id = document.getElementById('_id').value;
  hl.innerHTML = document.getElementById('_text').value;
  JSPcloseWindow('ardzon_modal_win');
}

// Removes an absolute URL part
function cutAbsUrl(url)
{
  cut_str = location.href.substr(0, location.href.lastIndexOf("/") + 1);
  var f = url.indexOf(cut_str);
  if (f != -1) {
    url = url.substr(0,f) + url.substr(f+cut_str.length);
  }

  cut_str = location.protocol+"//"+location.host;
  var f = url.indexOf(cut_str);
  if (f != -1)
    url = url.substr(0,f) + url.substr(f+cut_str.length);

  return url;
}
