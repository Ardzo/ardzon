/**
* WYSIWYG Editor "Ardzon"
*
* Library for work with pictures
*
* @copyright Copyright (C) 2006 - 2018 Richter
* @author Richter (richter@wpdom.com)
*/

// Returns selected image
function getImage(field_id)
{
  if (isGecko) {
    sel = document.getElementById('ardzon_'+field_id+'_frame').contentWindow.getSelection();

    if (sel && sel.rangeCount > 0) {
      range = sel.getRangeAt(0);
      if (range.startContainer.nodeType == 1)
      {
        var aControl = range.startContainer.childNodes[range.startOffset];
        if (aControl && aControl.tagName && aControl.tagName.toLowerCase() == 'img')
          return aControl;
      }
    } 
  } else {
    if (EditField[field_id].selection.type == "Control") {
      var tControl = EditField[field_id].selection.createRange();
      if (tControl(0).tagName == 'IMG') return tControl(0);
    }
  }
  return(null);
}

// Updates an image (transmitter)
function updateImage(field_id)
{
  width = (document.getElementById('img_width').value)?(document.getElementById('img_width').value):'';
  height = (document.getElementById('img_height').value)?(document.getElementById('img_height').value):'';
  if (Img.style.width != '') Img.style.width = width;
  else Img.width = width;
  if (Img.style.height != '') Img.style.height = height;
  else Img.height = height;

  Img.alt = (document.getElementById('img_alt').value)?(document.getElementById('img_alt').value):'';
  Img.className = (document.getElementById('img_class').value)?(document.getElementById('img_class').value):'';
  Img.id = (document.getElementById('img_id').value)?(document.getElementById('img_id').value):'';
}

// Removes an absolute URL part
function cutAbsUrlImg(url)
{
//  cut_str = location.href.substr(0, location.href.lastIndexOf("/") + 1);
//  var f = url.indexOf(cut_str);
  //if (f != -1) {
//    url = url.substr(0,f) + url.substr(f+cut_str.length);
  //}

  cut_str = location.protocol+"//"+location.host;
  var f = url.indexOf(cut_str);
  if (f != -1)
    url = url.substr(0,f) + url.substr(f+cut_str.length);

  return url;
}
