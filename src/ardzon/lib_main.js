/**
* WYSIWYG Editor "Ardzon"
*
* Main library and scripts
*
* @copyright Copyright (C) 2006 - 2018 Richter
* @author Richter (richter@wpdom.com)
*/

//******************************* Settings ************************************
var lng = 'en';
var ardzon_shield_server_code = true;
var ardzon_shield_js_code     = true;

var ardzon_path          = 'ardzon/';
var ardzon_path_pics     = 'ardzon/themes/native/pics/';
var ardzon_server_script = 'ardzon/main.php';
var ardzon_internal_css  = 'ardzon/work_area.css';
var ardzon_external_css  = '';
var ardzon_styles        = []; // Classes, available in style-select (array with couples "class-description")
                               // for example: ['class_1|Class one','second-class|Class Two']

// Uncomment, if use editor with SBS "Web-Pro"
//var ardzon_path          = '/sbs_wp/ardzon/';
//var ardzon_path_pics     = '/sbs_wp/ardzon/themes/native/pics/';
//var ardzon_server_script = '/sbs_wp/ardzon/main.php';
//var ardzon_internal_css  = '/sbs_wp/ardzon/work_area.css';
//var ardzon_external_css  = '/sbs_wp/themes/main/main.css';
//var ardzon_styles        = [];
//*************** End Settings. DON'T change anything below *******************

document.write('<script type="text/javascript" src="'+ardzon_path+'vendor/ardzo_jsplus.js"></script>');
document.write('<script type="text/javascript" src="'+ardzon_path+'vendor/jquery-3.3.1.min.js"></script>');
document.write('<script type="text/javascript" src="'+ardzon_path+'vendor/jquery.form.min.js"></script>');

document.addEventListener("DOMContentLoaded", function(event) { 
  // JQuery not loaded now, using a native JavaScript instead of "(document).ready("

  ServerScriptInsertionBegin    = '<div class="ardzon_server_script">'+Lng._LNG_ARDZON_SSI_WARNING_+'<span>';
  ServerScriptInsertionBegin_IE = '<div class=ardzon_server_script>'+Lng._LNG_ARDZON_SSI_WARNING_+'<span'; // Specially without quotes
  ServerScriptInsertionEnd      = 'SSI_end</span></div>';
  JSScriptInsertionBegin        = '<div class="ardzon_js_script">'+Lng._LNG_ARDZON_JSI_WARNING_+'<span>';
  JSScriptInsertionEnd          = 'JSI_end</span></div>';
});

document.write('<script src="'+ardzon_path+'lang/lng_'+lng+'.js" type="text/javascript"></script>');

document.write('<script type="text/javascript" src="'+ardzon_path+'lib_context_menu.js"></script>');
document.write('<script type="text/javascript" src="'+ardzon_path+'lib_tables.js"></script>');
document.write('<script type="text/javascript" src="'+ardzon_path+'lib_images.js"></script>');
document.write('<script type="text/javascript" src="'+ardzon_path+'lib_hyperlinks.js"></script>');

if (!Editor_List) var Editor_List = new Array();
if (!EditField) var EditField = new Array();

// Styles and start content
var EditFieldHeader = '<html><head>' +
  '<link href="'+ardzon_internal_css+'" rel="STYLESHEET" type="text/css" />' + 
  (ardzon_external_css?'<link href="'+ardzon_external_css+'" rel="STYLESHEET" type="text/css" />':'') +
  '</head><body>';
var EditFieldFooter = '</body></html>';

// Main object "Ardzon"
var Ardzon = {
  Guidelines : false, // Indicator
  CurSelection : null, // Current selection

  // Copy a selected fragment to buffer
  copyToBuffer: function(field_id) {
    if (document.getElementById('ardzon_'+field_id+'_frame').contentWindow.getSelection()) {
      tmp_1 = document.getElementById('ardzon_'+field_id+'_frame').contentWindow.getSelection().toString();
      $('body').append('<div id="ardzon_tmp_div">'+tmp_1+'</div>');
      var range = document.createRange();
      range.selectNodeContents(document.getElementById('ardzon_tmp_div'));
      window.getSelection().addRange(range);
      document.execCommand("Copy");

      $('#ardzon_tmp_div').remove();
    }
  },

  // Processes all actions of user
  processAction : function(TStyle, field_id, exec_arg) {
    content_prefix_1 = ' \
        <a href="javascript:JSPcloseWindow(\'ardzon_modal_win\')" title="'+Lng._LNG_ARDZON_CLOSEWINDOW_+'" class="close_window">X</a> \
    ';

    if (TStyle.match(/filterpaste/i)) {
      if (!isGecko) {
        this.CurSelection = EditField[field_id].selection.createRange();
      }
      JSPopenWindow("ardzon_modal_win", "", "center", 150);

      content = ' \
        <h2>'+Lng._LNG_ARDZON_FILTERPASTE_+'</h2> \
        <form id="pastef"> \
        '+Lng._LNG_ARDZON_FILTERPASTE1_+' \
        <table width="100%"> \
        <input type="hidden" name="_text" /> \
        <tr><td><iframe id="ardzon_filterpaste" contenteditable="true" style="width:390px;height:230px;"></iframe></td></tr> \
        <tr><td><input type="checkbox" name="fword" checked="checked" /> '+Lng._LNG_ARDZON_FILTERPASTE2_+'</td></tr> \
        <tr><td><input type="checkbox" name="ffont" checked="checked" /> '+Lng._LNG_ARDZON_FILTERPASTE3_+'</td></tr> \
        <tr><td><input type="button" value="'+Lng._LNG_ARDZON_PASTE_+'" OnClick="document.forms[\'pastef\'].elements[\'_text\'].value = EditField[\'ardzon_filtertext\'].body.innerHTML; Ardzon.pasteF(\''+field_id+'\')"></td></tr> \
        </table></form> \
        \
        <script type="text/javascript"> \
              if (isIE) EditField["ardzon_filtertext"] = frames[\'ardzon_filterpaste\'].document; \
              else if (isGecko || isPresto) EditField["ardzon_filtertext"] = document.getElementById(\'ardzon_filterpaste\').contentDocument; \
              EditField["ardzon_filtertext"].designMode = \'On\'; \
              EditField["ardzon_filtertext"].write("<html><body></body></html>"); \
        </script> \
      ';

      $('#ardzon_modal_win').html(content_prefix_1 + content);

    } else if (TStyle.match(/insertimage/i)) {

      Img = getImage(field_id);
      if (Img) { // Updating selected image
        JSPopenWindow("ardzon_modal_win", "", "center", 150);

        content = ' \
          <h2>'+Lng._LNG_ARDZON_IMAGEPROP_+'</h2> \
          <FORM action="" id="imgform"> \
          <table width="100%"> \
          <tr><td>'+Lng._LNG_ARDZON_WIDTH_+'</td><td><input type="text" id="img_width" name="img_width" size="2" /></td></tr> \
          <tr><td>'+Lng._LNG_ARDZON_HEIGHT_+'</td><td><input type="text" id="img_height" name="img_height" size="2" /></td></tr> \
          <tr><td>"Alt"</td><td><input type="text" id="img_alt" name="img_alt" size="50" /></td></tr> \
          <tr><td>'+Lng._LNG_ARDZON_CLASS_+'</td><td><input size="10" id=img_class name=img_class value="" /></td></tr> \
          <tr><td>ID ("id")</td><td><input size="10" id=img_id name=img_id value="" /></td></tr> \
          <tr><td colspan="2"><input type="button" value="OK" onclick="updateImage(\''+field_id+'\')" /></td></tr> \
          </table> \
          </FORM> \
          \
          <script type="text/javascript"> \
                document.getElementById(\'img_width\').value = Img.width; \
                document.getElementById(\'img_height\').value = Img.height; \
                document.getElementById(\'img_alt\').value = Img.alt; \
                document.getElementById(\'img_class\').value = Img.className; \
                document.getElementById(\'img_id\').value = Img.id; \
          </script> \
        ';

        $('#ardzon_modal_win').html(content_prefix_1 + content);

      } else { // Uploading new image

        JSPopenWindow("ardzon_modal_win", "", "center", 150);

        content = ' \
          <h2>'+Lng._LNG_ARDZON_ADDINGIMAGE_+'</h2> \
          <form action="'+ardzon_server_script+'" method="POST" enctype="multipart/form-data" id="imgform"> \
          <input type="hidden" name="upload_image" value="1" /> \
          <input type="hidden" name="field_id" value="'+field_id+'" /> \
          <input type="file" name="image" /><p /> \
          <input type="submit" value="OK" /> \
          </form> \
          \
          \
          <script type="text/javascript"> \
          $(document).ready(function(){ \
            var options = { \
              target: "#ardzon_modal_win", \
              success: function() { \
                } \
            }; \
            $("#imgform").ajaxForm(options); \
          }); \
          </script> \
        ';

        $('#ardzon_modal_win').html(content_prefix_1 + content);
      }

    } else if (TStyle.match(/create_hyperlink/i)) {

      hl = this.getParentNode(field_id);

      // Content preparation
      if (!hl.href) link_text = document.getElementById('ardzon_'+field_id+'_frame').contentWindow.getSelection().toString();
      else link_text = hl.innerHTML;

      content = ' \
        <h2>'+Lng._LNG_ARDZON_LINKPROP_+'</h2> \
        <form id="hyperlink_form"><table width="100%"> \
        <tr><td>'+Lng._LNG_ARDZON_TEXT_+'</td><td><input size="35" id="_text" name=_text value="'+link_text+'" /></td></tr> \
        <tr><td>URL</td><td><input size="35" id="url" name=url value="" /></td></tr> \
        <tr><td>'+Lng._LNG_ARDZON_TARGET_+'</td><td> \
	<select id="_target" name="_target"> \
	<option value="">'+Lng._LNG_ARDZON_TARGET1_+' ("")</option> \
	<option value="_blank">'+Lng._LNG_ARDZON_TARGET2_+' ("_blank")</option> \
	</select> \
        </td></tr> \
        <tr><td>'+Lng._LNG_ARDZON_TITLE_+'</td><td><input size="20" id="_title" name=_title value="" /></td></tr> \
        <tr><td>'+Lng._LNG_ARDZON_CLASS_+'</td><td><input size="10" id="_class" name=_class value="" /></td></tr> \
        <tr><td>ID ("id")</td><td><input size="10" id="_id" name=_id value="" /></td></tr> \
        <tr><td colspan="2"> \
      ';
      if (!hl.href) content += '<input type="button" value="'+Lng._LNG_ARDZON_INSERT_+'" OnClick="AddHyperlink(\''+field_id+'\')">';
      if (hl.href) content += '<input type="button" value="OK" onclick="updateHyperlink(\''+field_id+'\')" />';
      content += ' \
        </td></tr> \
        </table></form> \
      ';
      if (hl.href) content += ' \
	<script type="text/javascript"> \
	tmp = cutAbsUrl(hl.href); \
        if (tmp.substr(0, 2) == \'~#\') tmp = tmp.substr(1); \
        document.getElementById(\'url\').value = tmp; \
        document.getElementById(\'_target\').value = hl.target; \
        document.getElementById(\'_title\').value = hl.title; \
        document.getElementById(\'_class\').value = hl.className; \
        document.getElementById(\'_id\').value = hl.id; \
	</script> \
        ';
      // End of Content preparation
      
      if (hl.href) { // Existing
        JSPopenWindow("ardzon_modal_win", "", "center", 150);

        $('#ardzon_modal_win').html(content_prefix_1 + content);
      } else { // New
        EditField[field_id].body.focus();
        if (isIE) {
          this.CurSelection = EditField[field_id].selection.createRange();
        }
        JSPopenWindow("ardzon_modal_win", "", "center", 150);

        $('#ardzon_modal_win').html(content_prefix_1 + content);
      }

    } else if (TStyle.match(/inserttable/i) || TStyle.match(/table_properties/i)) {

      if (TStyle.match(/inserttable/i)) {
        EditField[field_id].body.focus();
        if (!isGecko) {
          this.CurSelection = EditField[field_id].selection.createRange();
        }
      } else if (TStyle.match(/table_properties/i)) {
        Table = this.getParentNode(field_id, 'TABLE');
      }

      JSPopenWindow("ardzon_modal_win", "", "center", 150);

      // Content preparation
      content = ' \
        <h2>'+Lng._LNG_ARDZON_TABLEPROP_+'</h2> \
        <form id="tblf"><table width="100%"> \
        <tr><td>'+Lng._LNG_ARDZON_WIDTH_+' (%, px)</td><td><input size="15" id=t_width name=width value="100%" /></td></tr> \
      ';
      if (TStyle.match(/table_properties/i)) content += ' \
        <tr><td>'+Lng._LNG_ARDZON_HEIGHT_+' (%, px)</td><td><input size="15" id=t_height name=height value="" /></td></tr> \
        ';
      if (TStyle.match(/inserttable/i)) content += ' \
        <tr><td>'+Lng._LNG_ARDZON_COLSQ_+'</td><td><input size="15" id=cols name=cols value="3" /></td></tr> \
        <tr><td>'+Lng._LNG_ARDZON_ROWSQ_+'</td><td><input size="15" id=rows name=rows value="2" /></td></tr> \
        ';
      content += ' \
        <tr><td>'+Lng._LNG_ARDZON_BORDER_+'</td><td><input size="15" id=border name=border value="1" /></td></tr> \
        <tr><td>'+Lng._LNG_ARDZON_CELLP_+'</td><td><input size="15" id=padding name=padding value="2" /></td></tr> \
        <tr><td>'+Lng._LNG_ARDZON_CELLS_+'</td><td><input size="15" id=spacing name=spacing value="0" /></td></tr> \
        <tr><td>'+Lng._LNG_ARDZON_CLASS_+'</td><td><input size="10" id="_class" name=_class value="" /></td></tr> \
        <tr><td>ID ("id")</td><td><input size="10" id="_id" name=_id value="" /></td></tr> \
      ';
      if (TStyle.match(/inserttable/i)) content += '<tr><td colspan=2><br /><input type="button" value="'+Lng._LNG_ARDZON_INSERT_+'" OnClick="AddTable(\''+field_id+'\')"></td></tr>';
      if (TStyle.match(/table_properties/i)) {
        content += ' \
          <tr><td colspan="2"> \
          <input type="button" value="OK" onclick="updateTable(\''+field_id+'\')" /> \
          </td></tr> \
        ';
      }
      content += ' \
        </table></form> \
      ';
      if (TStyle.match(/table_properties/i)) content += ' \
	<script type="text/javascript"> \
        document.getElementById(\'t_width\').value = (Table.style.width)?Table.style.width:Table.width; \
        document.getElementById(\'t_height\').value = (Table.style.height)?Table.style.height:Table.height; \
        if (document.getElementById(\'t_height\').value == \'undefined\') document.getElementById(\'t_height\').value = \'\'; \
        document.getElementById(\'border\').value = Table.border; \
        document.getElementById(\'padding\').value = Table.cellPadding; \
        document.getElementById(\'spacing\').value = Table.cellSpacing; \
        document.getElementById(\'_class\').value = Table.className; \
        document.getElementById(\'_id\').value = Table.id; \
	</script> \
        ';
      // End of Content preparation

      $('#ardzon_modal_win').html(content_prefix_1 + content);

    } else if (TStyle.match(/insert_row_before/i)) {
      InsertRow(field_id, 'before');
    } else if (TStyle.match(/insert_row_after/i)) {
      InsertRow(field_id, 'after');
    } else if (TStyle.match(/delete_row/i)) {
      DeleteRow(field_id);
    } else if (TStyle.match(/insert_column_before/i)) {
      insertColumn(field_id, 'before');
    } else if (TStyle.match(/insert_column_after/i)) {
      insertColumn(field_id, 'after');
    } else if (TStyle.match(/delete_column/i)) {
      DeleteColumn(field_id);
    } else if (TStyle.match(/delete_table/i)) {
      DeleteTable(field_id);
    } else if (TStyle.match(/split_cell/i)) {
      SplitCell(field_id);
    } else if (TStyle.match(/set_style/i)) {
      this.setStyle(field_id);
    } else if (TStyle.match(/visual_aid/i)) {

      this.handleVisualAid(EditField[field_id].body);

    } else if (TStyle.match(/forecolor/i) || TStyle.match(/backcolor/i)) {

      JSPopenWindow("ardzon_modal_win", "", "center", 150);

      function hex_color_code(i) {
        i *= 51;
        if (i<16) return '0'+i.toString(16);
        else return i.toString(16);
      }

      // Content preparation
      if (TStyle.match(/forecolor/i)) content = '<h2>'+Lng._LNG_ARDZON_FORECOLOR_+'</h2>';
      else content = '<h2>'+Lng._LNG_ARDZON_BACKCOLOR_+'</h2>';

      content += ' \
        <form name="clr"> \
        <input type=text name=colr style="width:40px;height:30px" readonly>&nbsp; \
        <input type=text name=colr_hex style="width:60px" value="#FFFFFF"> \
        </form> \
        <p />\
        <table cellpadding="0" cellspacing="0" border="0"> \
      ';
      i = 0;
      for (r=0; r<6; r++) {
        for (g=0; g<6; g++) {
          for (b=0; b<6; b++) {
            if (i==0) content += '<tr>';
            colr = hex_color_code(r)+hex_color_code(g)+hex_color_code(b);

            if (TStyle.match(/forecolor/i)) tmp_1 = '\'forecolor\'';
            else tmp_1 = '(isGecko || isPresto)?\'hiliteColor\':\'backcolor\'';

            content += ' \
              <td style="width:10px; height:10px; background-color:#'+colr+'" onclick="EditField[\''+field_id+'\'].execCommand('+tmp_1+',false,\'#'+colr+'\'); JSPcloseWindow(\'ardzon_modal_win\');" \
              onmouseover="document.forms[\'clr\'].elements[\'colr\'].style.background=\'#'+colr.toUpperCase()+'\';document.forms[\'clr\'].elements[\'colr_hex\'].value=\'#'+colr.toUpperCase()+'\'"></td> \
            ';
            i++;
            if (i==36) {
              content += '</tr>';
              i=0;
            }
          }
        }
      }
      content += ' \
        </table> \
      ';
      // End of Content preparation

      $('#ardzon_modal_win').html(content_prefix_1 + content);

    } else if (TStyle.match(/f1/i)) {

      var content = '<a href="javascript:JSPcloseWindow(\'ardzon_modal_win\')" title="'+Lng._LNG_ARDZON_CLOSEWINDOW_+'" class="close_window">X</a>' +
        '<div id="about_win_content">' +
        '<h2>Ardzon. '+Lng._LNG_ARDZON_ABOUT1_+'</h2>' +
        '<p />' +
        'Version <strong>2.1.1.1</strong>' +
        '<p />' +
        'Copyright &#169; 2006 - 2018 Richter' +
        '<p />' +
        'Website: <a href="http://ardzon.ardzo.com" target="_blank">ardzon.ardzo.com</a><br />' +
        'E-mail: <a href="mailto:richter@wpdom.com">richter@wpdom.com</a>.' +
        '<h3>'+Lng._LNG_ARDZON_ABOUT2_+'</h3>' +
        Lng._LNG_ARDZON_ABOUT3_ +
        '<p />' +
        '<a href="http://ardzon.ardzo.com/ru/help.php" target="_blank">'+Lng._LNG_ARDZON_ABOUT4_+'</a>' +
        '</div>';

      JSPopenWindow("ardzon_modal_win", content, "center", 150);

    } else if (TStyle.match(/copy/i)) { // Copy with toolbar button

      this.copyToBuffer(field_id);

    } else if (TStyle.match(/cut/i)) { // Cut with toolbar button

      this.copyToBuffer(field_id);

      tmp_1 = document.getElementById('ardzon_'+field_id+'_frame').contentWindow.getSelection();
      rang = tmp_1.getRangeAt(0);
      tmp_1.removeAllRanges();
      rang.deleteContents();

    } else if (TStyle.match(/paste/i)) { // Paste with toolbar button

      EditField[field_id].body.focus();

      if (isGecko || isPresto) {
        alert(Lng._LNG_ARDZON_CLIPBOARD_MOZILLA_);
      } else {
        var PastedText = EditField[field_id].selection.createRange();
        PastedText.execCommand("Paste");
      }

    } else {

      EditField[field_id].execCommand(TStyle, false, exec_arg)

    }
  },

  // Writes toolbar button
  getToolbarButton : function (field_id, style, pos_x, title, exec_arg)
  {
    return '<div class="ardzon_b_div"><a href="javascript:void(0);" onclick="Ardzon.processAction(\''+style+'\', \''+field_id+'\', \''+exec_arg+'\')" title="'+title+'" style="background-image:url('+ardzon_path_pics+'buttons.gif); background-position: -'+pos_x+'px 50%"></a></div>';
  },

  //
  getToolbarDivider : function ()
  {
    return '<div class="ardzon_b_divider"></div>';
  },

  // Set borders
  setBorders : function (Node, restore)
  {
    if (isIE) {
      var oldW = Node.style.width;
      var oldH = Node.style.height;
      if ((!this.Guidelines && !restore) || (restore && this.Guidelines && Node.runtimeStyle.borderWidth=='')) {
        Node.runtimeStyle.borderWidth = "1px";
        Node.runtimeStyle.borderStyle = "dashed";
        Node.runtimeStyle.borderColor = "#777777";
      } else {
        Node.runtimeStyle.borderWidth = "";
        Node.runtimeStyle.borderStyle = "";
        Node.runtimeStyle.borderColor = "";
      }
    // Really needed?
      Node.style.width = oldW;
      Node.style.height = oldH;
    }
  },

  // Visual aid (borders)
  handleVisualAid : function (Node, restore, isChild)
  {
    if (!Node) return;
    switch (Node.nodeName) {
      case 'TABLE':
        Ardzon.setBorders(Node, restore);
        for (var y=0; y<Node.rows.length; y++) {
          for (var x=0; x<Node.rows[y].cells.length; x++) {
            Ardzon.setBorders(Node.rows[y].cells[x], restore);
          }
        }
        break;
      case 'A':
      case 'P':
      case 'DIV':
      case 'UL':
      case 'OL':
        Ardzon.setBorders(Node, restore);
        break;
    }
    if (Node.hasChildNodes()) {
      for (var I=0; I<Node.childNodes.length; I++)
        this.handleVisualAid(Node.childNodes[I], restore, true);
      }
    if (!restore && !isChild) { // Changing state
      if (!Ardzon.Guidelines) Ardzon.Guidelines = true;
      else Ardzon.Guidelines = false;
    }
  },

  // 
  insertNodeAtSelection: function(win, insertNode)
  {
    var sel = win.getSelection(); // Current selection

    var range = sel.getRangeAt(0);

    sel.removeAllRanges(); // Deselect all

    range.deleteContents(); // Remove content of current selection from document

    // Get location of current selection
    var container = range.startContainer;
    var pos = range.startOffset;

    // Make a new range for the new selection
    range = document.createRange();

    if (container.nodeType==3 && insertNode.nodeType==3) { // Inesrt text in text node
      container.insertData(pos, insertNode.nodeValue);

      // put cursor after inserted text
      range.setEnd(container, pos+insertNode.length);
      range.setStart(container, pos+insertNode.length);
    } else {
      var afterNode;
      if (container.nodeType==3) {
        // When inserting into a textnode we create 2 new textnodes
        // and put the insertNode in between

        var textNode = container;
        container = textNode.parentNode;
        var text = textNode.nodeValue;

        // Text before the split
        var textBefore = text.substr(0,pos);
        // Text after the split
        var textAfter = text.substr(pos);

        var beforeNode = document.createTextNode(textBefore);
        var afterNode = document.createTextNode(textAfter);

        // Insert the 3 new nodes before the old one
        container.insertBefore(afterNode, textNode);
        container.insertBefore(insertNode, afterNode);
        container.insertBefore(beforeNode, insertNode);

        // Remove the old node
        container.removeChild(textNode);
      } else { // Simply insert the node
        afterNode = container.childNodes[pos];
        container.insertBefore(insertNode, afterNode);
      }

      range.setEnd(afterNode, 0);
      range.setStart(afterNode, 0);
    }

    sel.addRange(range);
    
    win.getSelection().removeAllRanges();
  },

  // Changes a style-property of selection
  setStyle : function (field_id)
  {
    classname = document.getElementById(field_id+"_style").value;

    var editor = document.getElementById('ardzon_'+field_id+'_frame');
    
    if (editor.contentWindow.getSelection().rangeCount > 0) {
      var currentRange = editor.contentWindow.getSelection().getRangeAt(0);

      var parent = currentRange.commonAncestorContainer;
      if (parent.nodeType != 1)
        parent = currentRange.commonAncestorContainer.parentNode;
      
      if (parent && parent.tagName.toLowerCase() != "body" && parent.tagName.toLowerCase() != "html") {
        parent.className = classname; // On parent
      } else {
        // Create new container
        var newSpan = editor.contentDocument.createElement("SPAN");
        newSpan.className = classname;
        newSpan.appendChild(currentRange.cloneContents());
        this.insertNodeAtSelection(editor.contentWindow, newSpan);
      }
    }
  },

  // Return parent specified node for current selection
  // if A empty simply return parent node
  getParentNode : function (field_id, A, make_path)
  {
    var B;
    if (make_path) {
      var path = new Array;
      A = 'HTML';
    }

    if (isGecko || isPresto) {
      sel = document.getElementById('ardzon_'+field_id+'_frame').contentWindow.getSelection();
      range = sel.getRangeAt(0);
      B = range.startContainer.parentNode;
    } else { // IE
      var C = EditField[field_id].selection.createRange();
      if (EditField[field_id].selection.type=="Control") {
        for (i=0;i<C.length;i++) {
          if (C(i).parentNode) {
            B = C(i).parentNode;
            break;
          }
        }
      } else {
        B = C.parentElement();
      }
    }

    if (!A || A=='') return B;
    while (B&&B.nodeName!=A) {
      B = B.parentNode;
      if (make_path) path[path.length] = B.nodeName;
    }

    if (make_path) return path;
    else return B;
  },

  // Remove a useless tags from HTML-code
  cleanCode : function (html, actions)
  {
    var act1 = /1/;
    var act2 = /2/;
    if (act1.test(actions)) html = html.replace(/<\/?font[^>]*>/gi, "" ); // "font"
    if (act2.test(actions)) {
      html = html.replace(/<\/?SPAN[^>]*>/gi, "" ); // "span"
      html = html.replace(/<(\w[^>]*) class=([^ |>]*)([^>]*)/gi, "<$1$3") ; // "class"
      html = html.replace(/<(\w[^>]*) style="([^"]*)"([^>]*)/gi, "<$1$3") ; // "style"
      html = html.replace(/<(\w[^>]*) lang=([^ |>]*)([^>]*)/gi, "<$1$3"); // "lang"
      html = html.replace(/<\\?\?xml[^>]*>/gi, ""); // Remove XML elements and declarations
      html = html.replace(/<\/?\w+:[^>]*>/gi, "") ; // Remove Tags with XML namespace declarations: <o<img src="images/smilies/tongue.gif" border="0" alt="">></o<img src="images/smilies/tongue.gif" border="0" alt="">>
    }
    return html;
  },

  // Paste with filtering
  pasteF : function (field_id)
  {
    var _text = document.forms['pastef'].elements['_text'].value;
    if (document.forms['pastef'].elements['fword'].checked) _text = this.cleanCode(_text, '2');
    if (document.forms['pastef'].elements['ffont'].checked) _text = this.cleanCode(_text, '1');
    if (isGecko) {
      sel = document.getElementById('ardzon_'+field_id+'_frame').contentWindow.getSelection();
      range = sel.getRangeAt(0);
      tmp = range.startContainer.parentNode;
      sel.removeAllRanges();
      range.deleteContents();
      var newElement = document.createElement('span');
      newElement.innerHTML = _text;
      range.insertNode(newElement);
    } else { // IE
      Ardzon.CurSelection.pasteHTML(_text);
    }
    JSPcloseWindow('ardzon_modal_win');
  },

  // Shields server and JS scripts, anchor links
  shieldSpecials : function(text)
  {
    // Links with anchors only
    text = text.replace(/href="#/gi, 'href="~#');

    // Server scripts
    if (ardzon_shield_server_code) {
      text = text.replace(/<\?/gi, ServerScriptInsertionBegin);
      text = text.replace(/\?>/gi, ServerScriptInsertionEnd);
    }

    // JS scripts
    if (ardzon_shield_js_code) {
      text = text.replace(/<script(.*?)>/gi, JSScriptInsertionBegin+'~$1~');
      text = text.replace(/<\/script>/gi, JSScriptInsertionEnd);
    }

    return text;
  },

  // Unshields server and JS scripts, anchor links
  unshieldSpecials : function(text)
  {
    // Links with anchors only
    text = text.replace(/href="~#/gi, 'href="#');

    // Server scripts
    if (ardzon_shield_server_code) {
      if (isIE) text = text.replace(RegExp(ServerScriptInsertionBegin_IE, 'gi'), "<?");
      else text = text.replace(RegExp(ServerScriptInsertionBegin, 'gi'), "<?");
      text = text.replace(RegExp(ServerScriptInsertionEnd, 'gi'), '?>');
    }

    // JS scripts
    if (ardzon_shield_js_code) {
      text = text.replace(RegExp(JSScriptInsertionBegin+'~(.*?)~', 'gi'), '<script$1>');
      text = text.replace(RegExp(JSScriptInsertionEnd, 'gi'), '</script>');
    }

    return text;
  },

  // Shows a work area in HTML mode
  showHTML : function (field_id)
  {
    document.getElementById("ardzon_"+field_id+"_view_normal").style.display = "none";
    document.getElementById("ardzon_"+field_id+"_view_html").style.display = "";
    document.getElementById("ardzon_"+field_id+"_toolbar_content").style.visibility = "hidden";

    // Cut all absolute URLs from A
    var links = EditField[field_id].getElementsByTagName('A');
    if (links != null) {
      for (var i=0;i<links.length;i++)
      {
      	links[i].href = cutAbsUrl(links[i].href);
      }
    }
    // Cut all absolute URLs from IMG
    var links = EditField[field_id].getElementsByTagName('IMG');
    if (links != null) {
      for (var i=0;i<links.length;i++)
      {
      	links[i].src = cutAbsUrlImg(links[i].src);
      }
    }

    tmp = EditField[field_id].body.innerHTML;
    document.getElementById('ardzon_'+field_id+'_container').style.display = "none";

    tmp = this.unshieldSpecials(tmp);

    $('#'+field_id).val(tmp);
    $('#'+field_id).show();
  },

  // Shows a work area in normal mode
  showNormal : function (field_id)
  {
    document.getElementById("ardzon_"+field_id+"_view_normal").style.display = "";
    document.getElementById("ardzon_"+field_id+"_view_html").style.display = "none";
    document.getElementById("ardzon_"+field_id+"_toolbar_content").style.visibility = "visible";

    tmp = $('#'+field_id).val();
    $('#'+field_id).hide();

    tmp = this.shieldSpecials(tmp);

    EditField[field_id].body.innerHTML = tmp;
    document.getElementById('ardzon_'+field_id+'_container').style.display = "block";
    EditField[field_id].designMode = 'On'; // Specially for Firefox
    Ardzon.handleVisualAid(EditField[field_id].body, true); // Restoring visual aid
  },

  // Fill hidden fields by values of WYSIWYG fields
  savePrepare : function ()
  {
    for (var I=0;I<Editor_List.length;I++) {
      if (document.getElementById("ardzon_"+Editor_List[I]+"_view_html").style.display == 'none') {
        // Cut all absolute URLs from A
        var links = EditField[Editor_List[I]].getElementsByTagName('A');
        if (links != null) {
          for (var i=0;i<links.length;i++)
          {
            links[i].href = cutAbsUrl(links[i].href);
          }
        }
        // Cut all absolute URLs from IMG
        var links = EditField[Editor_List[I]].getElementsByTagName('IMG');
        if (links != null) {
          for (var i=0;i<links.length;i++)
          {
            links[i].src = cutAbsUrlImg(links[i].src);
          }
        }

        tmp = EditField[Editor_List[I]].body.innerHTML;

        tmp = this.unshieldSpecials(tmp);

        $('#'+Editor_List[I]).val(tmp);
      }
    }
  },

  // Main function for execute an editor
  run : function (field_id, add_params)
  {
    $(document).ready(function(){ 
      Ardzon.showEditor(field_id, add_params);
    });
  },
  
  // Outputs an editor
  // addparams: width, height. For example: {'width':'100%', 'height':'100'}
  showEditor : function (field_id, add_params)
  {
    var width, height;

    if (add_params) {
      width  = add_params['width'];
      height = add_params['height'];
    }

    if (!width || width.length == 0) 
      width = $('#'+field_id).outerWidth();
    if (!height || height.length == 0)
      height = $('#'+field_id).outerHeight();

    width_style = width;
    if ($.isNumeric(width)) width_style += 'px';
    height_style = height;
    if ($.isNumeric(height)) height_style += 'px';

    content = ' \
	<div id="ardzon_'+field_id+'_toolbar" class="ardzon_toolbar"> \
	<div id="ardzon_'+field_id+'_toolbar_content"> \
      ';	

    content += this.getToolbarButton(field_id, 'undo', 780, Lng._LNG_ARDZON_UNDO_);
    content += this.getToolbarButton(field_id, 'redo', 750, Lng._LNG_ARDZON_REDO_);
    content += this.getToolbarDivider();
    content += this.getToolbarButton(field_id, 'cut', 180, Lng._LNG_ARDZON_CUT_);
    content += this.getToolbarButton(field_id, 'copy', 150, Lng._LNG_ARDZON_COPY_);
    content += this.getToolbarButton(field_id, 'paste', 690, Lng._LNG_ARDZON_PASTE_);
    content += this.getToolbarButton(field_id, 'filterpaste', 270, Lng._LNG_ARDZON_FILTERPASTE_);
    content += this.getToolbarDivider();
    content += this.getToolbarButton(field_id, 'create_hyperlink', 0, Lng._LNG_ARDZON_HREF_);
    content += this.getToolbarButton(field_id, 'inserthorizontalrule', 420, Lng._LNG_ARDZON_HR_);
    content += this.getToolbarButton(field_id, 'inserttable', 930, Lng._LNG_ARDZON_TABLE_);
    content += this.getToolbarButton(field_id, 'insertimage', 480, Lng._LNG_ARDZON_IMAGE_);
    content += this.getToolbarDivider();
    content += this.getToolbarButton(field_id, 'forecolor', 120, Lng._LNG_ARDZON_FORECOLOR_);
    content += this.getToolbarButton(field_id, 'backcolor', 90, Lng._LNG_ARDZON_BACKCOLOR_);
    content += this.getToolbarDivider();

    if (isIE) content += this.getToolbarButton(field_id, 'visual_aid', 1290, Lng._LNG_ARDZON_VISUAL_AID_);
    content += this.getToolbarButton(field_id, 'removeformat', '210', Lng._LNG_ARDZON_REMOVEFORMAT_);
    content += this.getToolbarDivider();

    content += this.getToolbarButton(field_id, 'FormatBlock', 300, 'H1', '<h1>');
    content += this.getToolbarButton(field_id, 'FormatBlock', 330, 'H2', '<h2>');
    content += this.getToolbarButton(field_id, 'FormatBlock', 360, 'H3', '<h3>');
    content += this.getToolbarButton(field_id, 'FormatBlock', 390, 'H4', '<h4>');
    content += this.getToolbarButton(field_id, 'FormatBlock', 660, Lng._LNG_ARDZON_PARAGRAPH_, '<p>');

    content += ' \
		<div style="clear: both"></div> \
		\
		<div class="ardzon_b_div"> \
    ';
    if (ardzon_styles.length > 0) {
      content += ' \
			<select id="'+field_id+'_style" onchange="Ardzon.processAction(\'set_style\', \''+field_id+'\')"> \
				<option value="">---</option> \
      ';
      ardzon_styles.forEach(function(item, i, ardzon_styles) {
        tmp = item.split('|');  
        content += '<option value="'+tmp[0]+'">'+tmp[1]+'</option>';
      });
      content += ' \
			</select> \
      ';
    }
    content += ' \
			<select id="'+field_id+'_fface" onchange="Ardzon.processAction(\'fontname\', \''+field_id+'\', document.getElementById(\''+field_id+'_fface\').value)"> \
				<option value="Arial" style="font-family: Arial">Arial</option> \
				<option value="Courier New" style="font-family: Courier New">Courier New</option> \
				<option value="Tahoma" style="font-family: Tahoma">Tahoma</option> \
				<option value="Times New Roman" style="font-family: Times New Roman">Times New Roman</option> \
				<option value="Verdana" selected="selected" style="font-family: Verdana">Verdana</option> \
			</select> \
			<select id="'+field_id+'_fsize" onchange="Ardzon.processAction(\'fontsize\', \''+field_id+'\', document.getElementById(\''+field_id+'_fsize\').value)"> \
				<option value="1">1</option> \
				<option value="2" selected="selected">2</option> \
				<option value="3">3</option> \
				<option value="4">4</option> \
				<option value="5">5</option> \
				<option value="6">6</option> \
				<option value="7">7</option> \
			</select> \
		</div> \
      ';	

    content += this.getToolbarButton(field_id, 'bold', 30, Lng._LNG_ARDZON_BOLD_);
    content += this.getToolbarButton(field_id, 'italic', 450, Lng._LNG_ARDZON_ITALIC_);
    content += this.getToolbarButton(field_id, 'underline', 1230, Lng._LNG_ARDZON_UNDERLINE_);
    content += this.getToolbarButton(field_id, 'strikethrough', 810, Lng._LNG_ARDZON_STRIKETHROUGH_);
    content += this.getToolbarButton(field_id, 'superscript', 900, Lng._LNG_ARDZON_SUP_);
    content += this.getToolbarButton(field_id, 'subscript', 870, Lng._LNG_ARDZON_SUB_);
    content += this.getToolbarDivider();
    content += this.getToolbarButton(field_id, 'justifyleft', 600, Lng._LNG_ARDZON_JUSTIFYLEFT_);
    content += this.getToolbarButton(field_id, 'justifycenter', 60, Lng._LNG_ARDZON_JUSTIFYCENTER_);
    content += this.getToolbarButton(field_id, 'justifyright', 720, Lng._LNG_ARDZON_JUSTIFYRIGHT_);
    content += this.getToolbarButton(field_id, 'justifyfull', 570, Lng._LNG_ARDZON_JUSTIFYFULL_);
    content += this.getToolbarDivider();
    content += this.getToolbarButton(field_id, 'insertorderedlist', 630, Lng._LNG_ARDZON_OL_);
    content += this.getToolbarButton(field_id, 'insertunorderedlist', 1260, Lng._LNG_ARDZON_UL_);
    content += this.getToolbarButton(field_id, 'indent', 510, Lng._LNG_ARDZON_INDENT_);
    content += this.getToolbarButton(field_id, 'outdent', 540, Lng._LNG_ARDZON_OUTDENT_);
    content += this.getToolbarDivider();
    content += this.getToolbarButton(field_id, 'f1', '240', Lng._LNG_ARDZON_F1_);

    content += ' \
		<div style="clear: both"></div> \
	</div> \
	</div> \
	\
	<div id="ardzon_'+field_id+'_container"> \
		<iframe id="ardzon_'+field_id+'_frame" width="'+width+'" height="'+height+'" contenteditable="true" class="ardzon_editable_iframe"></iframe> \
	</div>	\
      ';

    content_1 = ' \
	<div id="ardzon_'+field_id+'_view_normal"> \
		<img name="Normal" src="'+ardzon_path_pics+'view_normal.gif" width="108" height="17" border="0" usemap="#m_Normal_'+field_id+'" alt="" /> \
		<map name="m_Normal_'+field_id+'"> \
		<area shape="poly" coords="59,1,56,8,59,15,99,15,105,3,104,1,59,1" href="javascript:Ardzon.showHTML(\''+field_id+'\');"> \
		</map> \
	</div> \
	<div id="ardzon_'+field_id+'_view_html" style="display:none;"> \
		<img name="HTML" src="'+ardzon_path_pics+'view_html.gif" width="108" height="17" border="0" usemap="#m_HTML_'+field_id+'" alt="" /> \
		<map name="m_HTML_'+field_id+'"> \
		<area shape="poly" coords="1,1,51,0,55,9,53,15,8,15,2,3,1,1" href="javascript:Ardzon.showNormal(\''+field_id+'\');"> \
		</map> \
	</div> \
	\
      ';

    $('#'+field_id).before(content);
    $('#'+field_id).after(content_1);
    $('#'+field_id).hide();
    $('#'+field_id).css('width', width_style);
    $('#'+field_id).css('height', height_style);
    $('#'+field_id).addClass('ardzon_editable_textarea');

    $('#ardzon_'+field_id+'_toolbar').css('width', width_style);

    // Action on "submit" for form
    $('#'+field_id).closest('form').submit(function() {
      Ardzon.savePrepare();
    });

    // Initialization of an object

    field_value = $('#'+field_id).val().trim();

    field_value = this.shieldSpecials(field_value);

    if (!isIE && !isGecko && !isPresto) { // If not supported
      document.getElementById("ardzon_"+field_id+"_toolbar").style.display = 'none';
      document.getElementById('ardzon_'+field_id+'_container').style.display = "none";
      document.getElementById(field_id).style.display = "block";
      document.getElementById(field_id).value = field_value;
      document.getElementById('ardzon_'+field_id+'_view_normal').style.display = "none";
      document.getElementById('ardzon_'+field_id+'_view_html').style.display = "none";
    } else {
      if (isIE) EditField[field_id] = frames['ardzon_'+field_id+'_frame'].document;
      else if (isGecko || isPresto) EditField[field_id] = document.getElementById('ardzon_'+field_id+'_frame').contentDocument;

      EditField[field_id].designMode = 'On';

      EditField[field_id].open();
      EditField[field_id].write(EditFieldHeader);
      EditField[field_id].write(field_value);
      EditField[field_id].write(EditFieldFooter);
      EditField[field_id].close();

      try { // "try" for IE
        EditField[field_id].execCommand('useCSS',true,'true'); // For "hiliteColor"
      } catch(e) {}

      if (isGecko || isPresto) EditField[field_id].addEventListener('contextmenu', function(event){ContextMenu.Build(event, field_id)}, true);
      if (isIE) EditField[field_id].oncontextmenu = function() { return ContextMenu.Build(event, field_id); }

      // Registration
      Editor_List[Editor_List.length] = field_id;
    }

  }
}
// End of object "Ardzon"
