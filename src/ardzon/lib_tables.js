/**
* WYSIWYG Editor "Ardzon"
*
* Library for work with tables
*
* @copyright Copyright (C) 2006 - 2018 Richter
* @author Richter (richter@wpdom.com)
*/

// Addes a table
function AddTable(field_id)
{
  var newtable		= document.createElement('TABLE');
  newtable.style.width	= document.forms['tblf'].elements['width'].value;
  newtable.border	= document.forms['tblf'].elements['border'].value;
  newtable.cellPadding	= document.forms['tblf'].elements['padding'].value;
  newtable.cellSpacing	= document.forms['tblf'].elements['spacing'].value;
  newtable.className    = document.forms['tblf'].elements['_class'].value;
  newtable.id           = document.forms['tblf'].elements['_id'].value;
  
  for (var i=0;i<document.forms['tblf'].elements['rows'].value;i++)
  {
    var newrow = document.createElement('TR');
    for (var j=0; j<document.forms['tblf'].elements['cols'].value; j++)
    {
      var newcell = document.createElement('TD');
      newrow.appendChild(newcell);
    }
    newtable.appendChild(newrow);
  }

  if (isGecko) {
    sel = document.getElementById('ardzon_'+field_id+'_frame').contentWindow.getSelection();
    range = sel.getRangeAt(0);
    tmp = range.startContainer.parentNode;
    sel.removeAllRanges();
    range.deleteContents();
    range.insertNode(newtable);
  } else { // IE
    Ardzon.CurSelection.pasteHTML(newtable.outerHTML);
  }
  JSPcloseWindow('ardzon_modal_win');
}

// Updates a table
function updateTable(field_id)
{
  Table.style.width  = (document.getElementById('t_width').value)?(document.getElementById('t_width').value):'';
  Table.style.height = (document.getElementById('t_height').value)?(document.getElementById('t_height').value):'';
  Table.border       = (document.getElementById('border').value)?(document.getElementById('border').value):'';
  Table.cellPadding  = document.getElementById('padding').value;
  Table.cellSpacing  = document.getElementById('spacing').value;
  Table.className    = document.getElementById('_class').value;
  Table.id           = document.getElementById('_id').value;
}

// Addes a row in table
// "where_paste": before or after
function InsertRow(field_id, where_paste)
{
  var newRow = EditField[field_id].createElement("tr");
  exist_tr = Ardzon.getParentNode(field_id, 'TR', false);

  if (where_paste == 'before') {
    exist_tr.parentNode.insertBefore(newRow,exist_tr);
    for (var i=0; i<exist_tr.cells.length; i++) {
      var newCell = exist_tr.cells[i].cloneNode(false);
      newRow.appendChild(newCell);
    }
  } else if (where_paste == 'after') {
    exist_tr.parentNode.insertBefore(newRow,exist_tr.nextSibling);
    for (var i=0; i<exist_tr.cells.length; i++) {
      if (exist_tr.cells[i].rowSpan > 1) {
        exist_tr.cells[i].rowSpan++;
      } else {
        var newCell = exist_tr.cells[i].cloneNode(false);
        newRow.appendChild(newCell);
      }
    }
    exist_table = Ardzon.getParentNode(field_id, 'TABLE', false);
    for (var i=0; i<exist_tr.rowIndex; i++) {
      var tempr = exist_table.rows[i];
      for (var j=0; j<tempr.cells.length; j++) {
        if (tempr.cells[j].rowSpan > (exist_tr.rowIndex - i))
          tempr.cells[j].rowSpan++;
      }
    }
  }
}

// Deletes a row
function DeleteRow(field_id)
{
  exist_tr = Ardzon.getParentNode(field_id, 'TR', false);
  exist_table = Ardzon.getParentNode(field_id, 'TABLE', false);
  tbody = Ardzon.getParentNode(field_id, 'TBODY', false);
  if (tbody.children.length == 1) {
    Ardzon.getParentNode(field_id, 'TABLE', false).removeNode(true);
  } else {
    var matrix = getTableMatrix(exist_table);

    // Upper rows
    for (var i=0; i<exist_tr.rowIndex; i++) {
      var tempr = exist_table.rows[i];
      for (var j=0; j<tempr.cells.length; j++) {
        if (tempr.cells[j].rowSpan > (exist_tr.rowIndex - i))
          tempr.cells[j].rowSpan--;
      }
    }
    
    curCI = -1;
    // Check for current row cells spanning more than 1 row
    for (var i=0; i<matrix[exist_tr.rowIndex].length; i++)
    {
      prevCI = curCI;
      curCI = matrix[exist_tr.rowIndex][i];
      if (curCI != -1 && curCI != prevCI && exist_tr.cells[curCI].rowSpan>1 && (exist_tr.rowIndex+1)<exist_table.rows.length) {
        ni = i;
        nrCI = matrix[exist_tr.rowIndex+1][ni];
        while (nrCI == -1) {
          ni++;
          if (ni<exist_table.rows[exist_tr.rowIndex+1].cells.length)
            nrCI = matrix[exist_tr.rowIndex+1][ni];
          else
            nrCI = exist_table.rows[exist_tr.rowIndex+1].cells.length;
        }
        
        var newc = exist_table.rows[exist_tr.rowIndex+1].insertCell(nrCI);
        exist_table.rows[exist_tr.rowIndex].cells[curCI].rowSpan--;
        var nc = exist_table.rows[exist_tr.rowIndex].cells[curCI].cloneNode(false);
        newc.parentNode.replaceChild(nc, newc);

        // Fix the matrix
        cs = (exist_tr.cells[curCI].colSpan>1)?exist_tr.cells[curCI].colSpan:1;
        for (var j=i; j<(i+cs);j++) {
          matrix[exist_tr.rowIndex+1][j] = nrCI;
          nj = j;
        }
        for (var j=nj; j<matrix[exist_tr.rowIndex+1].length; j++) {
          if (matrix[exist_tr.rowIndex+1][j] != -1)
            matrix[exist_tr.rowIndex+1][j]++;
        }
      }
    }
    exist_table.deleteRow(exist_tr.rowIndex);
  }
}

// Addes a column
// "where_paste": "before" or "after"
function insertColumn(field_id, where_paste)
{
  if (isGecko) {
    sel = document.getElementById('ardzon_'+field_id+'_frame').contentWindow.getSelection();
    range = sel.getRangeAt(0);
    if (range.startContainer.tagName == 'TD') exist_td = range.startContainer;
    else exist_td = Ardzon.getParentNode(field_id, 'TD', false);
  } else { // IE
    exist_td = Ardzon.getParentNode(field_id, 'TD', false);
  }

  exist_tr = Ardzon.getParentNode(field_id, 'TR', false);
  exist_table = Ardzon.getParentNode(field_id, 'TABLE', false);

  matrix = getTableMatrix(exist_table);
  realIndex = getRealIndex(matrix, exist_tr, exist_td);

  for (var i=0; i<exist_table.rows.length; i++) {
    if (matrix[i][realIndex] != -1) {
      if (exist_table.rows[i].cells[matrix[i][realIndex]].colSpan > 1) {
        exist_table.rows[i].cells[matrix[i][realIndex]].colSpan++;
      } else {
        var newc = exist_table.rows[i].insertCell(matrix[i][realIndex]+1);
        var nc = exist_table.rows[i].cells[matrix[i][realIndex]].cloneNode(false);
        newc.parentNode.replaceChild(nc, newc);
      }
    }
  }
/*
  for (var y=0; y<table.rows.length; y++) {
    var newCell = EditField[field_id].createElement("td");
    if (where_paste == 'before')
      table.rows[y].cells[exist_td.cellIndex].parentNode.insertBefore(newCell,table.rows[y].cells[exist_td.cellIndex]);
    else if (where_paste == 'after')
      table.rows[y].cells[exist_td.cellIndex].parentNode.insertBefore(newCell,table.rows[y].cells[exist_td.cellIndex].nextSibling);
  }
*/
}

// Deletes a column
function DeleteColumn(field_id)
{
  if (isGecko) {
    sel = document.getElementById('ardzon_'+field_id+'_frame').contentWindow.getSelection();
    range = sel.getRangeAt(0);
    if (range.startContainer.tagName == 'TD') exist_td = range.startContainer;
    else exist_td = Ardzon.getParentNode(field_id, 'TD', false);
  } else { // IE
    exist_td = Ardzon.getParentNode(field_id, 'TD', false);
  }

  exist_tr = Ardzon.getParentNode(field_id, 'TR', false);
  exist_table = Ardzon.getParentNode(field_id, 'TABLE', false);

  matrix = getTableMatrix(exist_table);

  if (matrix[0].length<=1) {
    exist_table.removeNode(true);
  } else {
    realIndex = getRealIndex(matrix, exist_tr, exist_td);
    
    for (var i=0; i<exist_table.rows.length; i++) {
      if (matrix[i][realIndex] != -1) {
        if (exist_table.rows[i].cells[matrix[i][realIndex]].colSpan > 1)
          exist_table.rows[i].cells[matrix[i][realIndex]].colSpan--;
        else
          exist_table.rows[i].deleteCell(matrix[i][realIndex]);
      }
    }
  }
}

// Deletes a table
function DeleteTable(field_id)
{
  exist_table = Ardzon.getParentNode(field_id, 'TABLE', false);
  if (isGecko) exist_table.parentNode.removeChild(exist_table);
  else exist_table.removeNode(true);
}

// Split a cell
function SplitCell(field_id)
{
  var newCell = EditField[field_id].createElement("td");
  exist_td = Ardzon.getParentNode(field_id, 'TD', false);
  exist_td.parentNode.insertBefore(newCell,exist_td);
}

// Return table matrix
function getTableMatrix(table)
{
  var matrix = new Array();
  for (var i=0; i<table.rows.length; i++)
    matrix[i]=new Array();
  for (var i=0; i<table.rows.length; i++) {
    jr = 0;
    for (var j=0; j<table.rows[i].cells.length; j++) {
      while (matrix[i][jr] != undefined) 
        jr++;
      for (var jh=jr; jh<jr+(table.rows[i].cells[j].colSpan?table.rows[i].cells[j].colSpan:1); jh++) {
        for (var jv=i; jv<i+(table.rows[i].cells[j].rowSpan?table.rows[i].cells[j].rowSpan:1); jv++) {
          if (jv==i) {
            matrix[jv][jh] = table.rows[i].cells[j].cellIndex;
          } else {
            matrix[jv][jh]=-1;
          }
        }
      }
    }
  }
  return(matrix);
}

// Return real index of cell
function getRealIndex(matrix, exist_tr, exist_td)
{
  for (var j=0; j<matrix[exist_tr.rowIndex].length; j++) {
    if (matrix[exist_tr.rowIndex][j] == exist_td.cellIndex) {
      realIndex = j;
      break;
    }
  }
  return realIndex;
}