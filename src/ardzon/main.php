<?php
/**
* WYSIWYG Editor "Ardzon"
*
* Server part of editor
*
* @copyright Copyright (C) 2006 - 2018 Richter
* @author Richter (richter@wpdom.com)
*/

error_reporting(0);

//******************************* Settings ************************************
$field_id = @$_POST['field_id'];
$upload_image = @$_POST['upload_image'];
$ardzon_data_folder = '/data/'; // Folder for uploading a pictures
//*************************** Section of actions ******************************
// Uploading an image
if ($upload_image) {
    $pic_id = uniqid('');

    // Getting a extension
    preg_match("/\.([^\.]*?)$/", $_FILES['image']['name'], $ext);
    $ext = strtolower($ext[0]);

    move_uploaded_file($_FILES['image']['tmp_name'], $_SERVER['DOCUMENT_ROOT'].$ardzon_data_folder.$pic_id.$ext);
    @chmod($_SERVER['DOCUMENT_ROOT'].$ardzon_data_folder.$pic_id.$ext, 0777);

    echo '
	<script type="text/javascript">
	EditField["'.$field_id.'"].execCommand("insertimage", false, "'.$ardzon_data_folder.$pic_id.$ext.'");
	JSPcloseWindow(\'ardzon_modal_win\');
	</script>
    ';
    exit;
}
//************************ End of Section of actions **************************
?>
