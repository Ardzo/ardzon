/*******************************************************************************
Ardzo.JSPlus. V0.5.2. Copyright (C) 2008 - 2010 Richter
Additional information: http://wpdom.com, richter@wpdom.com
*******************************************************************************/

// Detecting of a browser
if (navigator.userAgent.match(/gecko/i)) isGecko = true; else isGecko = false;
if (navigator.userAgent.match(/presto/i)) isPresto = true; else isPresto = false;
if (navigator.userAgent.match(/msie/i)) isIE = true; else isIE = false;

// Set a cookie value
function JSPsetCookie(name, value) {
  expDate = new Date();
  expDate.setYear(expDate.getFullYear() + 1);
  document.cookie = name+"="+escape(value)+";path=/;expires="+expDate.toGMTString();
}

// Return value of cookie
function JSPgetCookie(name) {
  var prefix = name + "="
  var cookieStartIndex = document.cookie.indexOf(prefix)
  if (cookieStartIndex == -1)
    return null
  var cookieEndIndex = document.cookie.indexOf(";", cookieStartIndex + prefix.length)
  if (cookieEndIndex == -1)
    cookieEndIndex = document.cookie.length
  return unescape(document.cookie.substring(cookieStartIndex + prefix.length, cookieEndIndex))
}

// 
function JSPgetScrollY()
{
  return self.pageYOffset || (document.documentElement && document.documentElement.scrollTop) || (document.body && document.body.scrollTop);
}

// Opens a new window (add a node)
// x - "center" or digit
function JSPopenWindow(id, content, x, y, mover_id, dragDir, event)
{
  if (document.getElementById(id) == null) {
    var winContainer = document.createElement('div');
    document.body.appendChild(winContainer);
    winContainer.setAttribute('id', id);
    winContainer.style.position = 'absolute';

    document.getElementById(id).innerHTML = content;

    if (mover_id) {
      if (isIE) {
        document.getElementById(mover_id).onmousedown = function() {dragStart(event, id);}
//        document.getElementById(mover_id).onmouseup = function() {alert(winContainer.offsetLeft)}
      } else if (isGecko) {
        document.getElementById(mover_id).addEventListener('mousedown', function(event){dragStart(event, id)}, true);
//        document.getElementById(mover_id).addEventListener('mouseup', function(event){JSPsetCookie(id+"_coordX", winContainer.offsetLeft);JSPsetCookie(id+"_coordY", winContainer.offsetTop)}, true);
      }
    }
  }

  document.getElementById(id).style.display = "block"; // Needed for defining of width

  if (event) {
    if (navigator.userAgent.match(/gecko/i)) { x = x + event.clientX, y = y + event.clientY; }
    if (navigator.userAgent.match(/msie/i)) { x = x + window.event.x, y = y + window.event.y; }
  } else {
    if (x == 'center') x = (screen.width - document.getElementById(id).offsetWidth)/2;
  }

  y = y + JSPgetScrollY();

  document.getElementById(id).style.left = x + "px";
  document.getElementById(id).style.top = y + "px";
}

// Close a window (remove)
function JSPcloseWindow(id)
{
  document.getElementById(id).parentNode.removeChild(document.getElementById(id));
}

// Shows or hides a block
function JSPswitchHiddableBlock(id)
{
  if (document.getElementById(id).style.display == "none") {
    document.getElementById(id).style.display = "block";
    JSPsetCookie(id, 'block');
    document.getElementById(id+"_b_open").style.display = "none";
    document.getElementById(id+"_b_close").style.display = "inline";
  } else {
    document.getElementById(id).style.display = "none";
    JSPsetCookie(id, 'none');
    document.getElementById(id+"_b_open").style.display = "inline";
    document.getElementById(id+"_b_close").style.display = "none";
  }
}

// Shows or hides a block, depending from cookie
function JSPcheckHiddableBlock(id)
{
  if (JSPgetCookie(id) == "block") {
    document.getElementById(id).style.display = "block";
    document.getElementById(id + "_b_open").style.display = "none";
    document.getElementById(id + "_b_close").style.display = "inline";
  } else {
    document.getElementById(id).style.display = "none";
    document.getElementById(id + "_b_open").style.display = "inline";
    document.getElementById(id + "_b_close").style.display = "none";
  }
}